import os
import json
from collections import defaultdict

from django.shortcuts import render
from django.conf import settings
from django.forms import model_to_dict

from c3po.models import Routes


def _best_result(departure, arrival, max_auto, routes, countdown, bounty_hunters):
    '''
    Compute the max probability to go from the departure planet to the arrival planet
    '''

    # get planet mapping to avoid searching in routes every time
    accessible_planets = defaultdict(list)
    for route in routes:
        planet_1 = route['origin']
        planet_2 = route['destination']
        cost = route['travel_time']
        accessible_planets[planet_1].append((planet_2, cost, cost))
        accessible_planets[planet_2].append((planet_1, cost, cost))

    # same with days and bounty
    days_to_bounty = {}
    for bounty in bounty_hunters:
        days_to_bounty[bounty['day']] = bounty['planet']

    def _compute_proba(planet, planet_empire, proba):
        if planet == planet_empire:
            return proba * 0.9
        else:
            return proba

    def _tree_path(planet, proba, auto, cd):
        if cd > countdown or auto < 0:
            return 0.
        elif planet == arrival:
            return proba
        elif cd == countdown:
            return 0.
        else:
            next_planets = accessible_planets[planet] + [(planet, auto - max_auto, 1)]

            # generates tuples containing info after next action:
            # (next planet, next proba, next autonomy, next countdown)
            next_path_gen = ((n[0], _compute_proba(n[0], days_to_bounty.get(cd + n[2], ''), proba),
                              auto - n[1], cd + n[2])
                             for n in next_planets)
            
            # get children's max, or return immediatly 1. if one child returns it
            best_res = 0.
            for pl, pr, au, c in next_path_gen:
                res = _tree_path(pl, pr, au, c)
                if res == 1.:
                    return 1.
                elif res > best_res:
                    best_res = res
            return best_res

    return _tree_path(departure, 1., max_auto, 0)


def check_empire_valid(empire_data):
    try:
        empire_data = json.load(empire_data)
    except ValueError:
        return None

    cd = empire_data.get('countdown', None)
    if not isinstance(cd, int):
        return None

    bounty = empire_data.get('bounty_hunters', None)
    if not isinstance(bounty, list):
        return None

    if any((not isinstance(b.get('planet', None), str))
            or (not isinstance(b.get('day', None), int))
            for b in bounty):
        return None

    return empire_data


def main_page(request):
    if request.method == 'GET':
        return render(request, 'index.html', context=None)

    if request.method == 'POST':
        # get backend json file
        json_file = os.path.join(settings.BASE_DIR, 'c3po', 'json_files', 'millenium-falcon.json')
        with open(json_file) as millenium_file:
            millenium_data = json.load(millenium_file)

        # get front end data
        empire_data = request.FILES.get('json_file', None)

        if empire_data is None:
            return render(request, 'index.html', context=None)

        # check if empire_data is valid
        empire_data = check_empire_valid(empire_data)
        if empire_data is None:
            return render(request, 'invalid_data.html', context=None)
        
        # modify db path
        routes_db = millenium_data['routes_db']
        old_db_path = settings.DATABASES['default']['NAME']
        settings.DATABASES['default']['NAME'] = os.path.join(settings.BASE_DIR, routes_db)
        
        # get db data
        routes_data = [model_to_dict(i) for i in Routes.objects.all()]
        
        best_proba = _best_result(millenium_data['departure'], millenium_data['arrival'],
                                  millenium_data['autonomy'], routes_data, empire_data['countdown'],
                                  empire_data['bounty_hunters'])
        
        # restore old db path
        settings.DATABASES['default']['NAME'] = old_db_path
        
        return render(request, 'results.html', {'millenium': millenium_data, 'routes': routes_data,
                                                'empire': empire_data, 'best_proba': best_proba * 100})

