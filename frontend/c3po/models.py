from django.db import models

# Create your models here.
class Routes(models.Model):
    origin = models.CharField(max_length=20)
    destination = models.CharField(max_length=20)
    travel_time = models.IntegerField()

