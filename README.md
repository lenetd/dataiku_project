**Dataiku application exercise from Dorian LE NET**

---

## json_files

Contains some json frontend example files.

---

## frontend

Contains the django application.

Run the following command to run the application:
```
python frontend/manage.py runserver
```

