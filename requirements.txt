pandas==0.23.4
numpy==1.15.2
python-dateutil==2.7.3
scipy==1.1.0
matplotlib==3.0.0
pygame==1.9.4
django==2.1.3
